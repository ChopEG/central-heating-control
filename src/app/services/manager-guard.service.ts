import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService, Roles } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ManagerGuardService implements CanActivate {

  constructor(public auth: AuthService) {}

  canActivate(): boolean {
    return this.auth.getRole() === Roles.Manager;
  }
}
