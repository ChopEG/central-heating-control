import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

export enum Roles {
  User = 'user',
  Manager = 'manager'
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private role: string;
  private token: string;

  constructor(private router: Router) { }

  public getRole(): string {
    return this.role;
  }

  public getToken(): string {
    return this.token;
  }

  public setRole(role: string): void {
    this.role = role;
    this.token = this.makeToken(16);
  }

  public makeToken(length): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }

  public logout(): void {
    this.router.navigate(['']);
  }
}
