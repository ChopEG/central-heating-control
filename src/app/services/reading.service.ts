import { Injectable } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { map, repeat } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { Reading } from '../interfaces/reading';
import { Pagination } from '../interfaces/main';


@Injectable({
  providedIn: 'root'
})
export class ReadingService {

  constructor(private authService: AuthService) { }

  private getRandomMinMax = (min: number, max: number): number => Math.round(Math.random() * (max - min) + min);
  private getRandomFloatMinMax = (min: number, max: number): number => Math.round((Math.random() * (max - min) + min) * 100) / 100;

  private generateReading = (thermostatId: string): Reading => {
    return {
      id: this.authService.makeToken(16),
      thermostat_id: thermostatId,
      temperature: this.getRandomFloatMinMax(20, 40),
      humidity: this.getRandomFloatMinMax(40, 90),
      battery_charge: this.getRandomMinMax(1, 20),
    };
  }

  // Every 10 second
  public oneByThermostatIdTimer(thermostatId: string): Observable<Reading> {
    return timer(10000).pipe(
      repeat(),
      map(() => {
        return this.generateReading(thermostatId);
      }
    ));
  }

  // ​GET ​/thermostats/[id]/measurements:
  public allByThermostatId(thermostatId: string, page: number, pageSize: number): Observable<Pagination<Reading>> {
    return Observable.create(observer => {
      const readings: Array<Reading> = JSON.parse(localStorage.getItem('reading')) || [];
      const result: Array<Reading> = readings
        .filter((item: Reading) => item.thermostat_id === thermostatId).reverse();
      observer.next(this.paginateArray(result, page, pageSize));
      observer.complete();
    });
  }

  public save(data: Reading): Observable<boolean> {
    return Observable.create(observer => {
      const reading: Array<Reading> = JSON.parse(localStorage.getItem('reading')) || [];
      reading.push(data);
      localStorage.setItem('reading', JSON.stringify(reading));
      observer.next(true);
      observer.complete();
    });
  }

  private paginateArray = <T>(collection: T[], page: number = 1, numItems: number = 10): Pagination<T> => {

    if (!Array.isArray(collection) ) {
      throw `Expect array and got ${typeof collection}`;
    }

    const currentPage = page;
    const perPage = numItems;
    const offset = (page - 1) * perPage;
    const paginatedItems = collection.slice(offset, offset + perPage);
    return {
      currentPage,
      perPage,
      total: collection.length,
      totalPages: Math.ceil(collection.length / perPage),
      data: paginatedItems as T[]
    };
  }
}
