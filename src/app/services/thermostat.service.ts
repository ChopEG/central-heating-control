import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Thermostat, ThermostatForm, ThermostatReading } from '../interfaces/thermostat';
import { AuthService } from './auth.service';

export enum Locations {
  LivingRoom = 'Living room',
  Bedroom = 'Bedroom',
  Hallway = 'Hallway',
  Lavatory = 'Lavatory',
  Bathroom = 'Bathroom'
}

@Injectable({
  providedIn: 'root'
})
export class ThermostatService {

  constructor(private authService: AuthService) { }

  public all(): Observable<Array<Thermostat>> {
    return Observable.create(observer => {
      const thermostat: Array<Thermostat> = JSON.parse(localStorage.getItem('thermostat')) || [];
      observer.next(thermostat);
      observer.complete();
    });
  }

  public allByFlat(flat: number): Observable<Array<ThermostatReading>> {
    return Observable.create(observer => {
      const thermostats: Array<Thermostat> = JSON.parse(localStorage.getItem('thermostat')) || [];
      const result: Array<ThermostatReading> = thermostats
        .filter((item: Thermostat) => item.household_token === flat)
        .map<ThermostatReading>((item: Thermostat) => {
          return {...item, reading: {
            temperature: null,
            humidity: null,
            battery_charge: null
          }};
      });
      observer.next(result);
      observer.complete();
    });
  }

  public one(id: string): Observable<Thermostat> {
    return Observable.create(observer => {
      const thermostats: Array<Thermostat> = JSON.parse(localStorage.getItem('thermostat')) || [];
      const result: Thermostat = thermostats.find((item: Thermostat) => item.id === id);
      observer.next(result);
      observer.complete();
    });
  }

  delete(id: string): Observable<boolean> {
    return Observable.create(observer => {
      const thermostats: Array<Thermostat> = JSON.parse(localStorage.getItem('thermostat')) || [];
      for (let i = 0; i < thermostats.length; i++) {
        if (thermostats[i].id === id) {
          thermostats.splice(i, 1);
        }
      }
      localStorage.setItem('thermostat', JSON.stringify(thermostats));
      observer.next(true);
      observer.complete();
    });
  }

  public save(data: ThermostatForm): Observable<boolean> {
    return Observable.create(observer => {
      const thermostat: Array<Thermostat> = JSON.parse(localStorage.getItem('thermostat')) || [];
      thermostat.push({ ...data, id: this.authService.makeToken(16) } as Thermostat);
      localStorage.setItem('thermostat', JSON.stringify(thermostat));
      observer.next(true);
      observer.complete();
    });
  }
}
