import { Component, OnDestroy, OnInit } from '@angular/core';
import { ThermostatService } from '../../services/thermostat.service';
import { ThermostatReading } from '../../interfaces/thermostat';
import { Observable, Subscription } from 'rxjs';
import { ReadingService } from '../../services/reading.service';
import { Reading } from '../../interfaces/reading';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-component',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit, OnDestroy {

  flat: number;
  errorText: string;
  thermostats: Array<ThermostatReading>;
  subscribes: Array<Subscription>;
  subscribeQueryParams: Subscription;

  constructor(private thermostatService: ThermostatService,
              private readingService: ReadingService,
              private router: Router,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.flat = null;
    this.errorText = '';
    this.thermostats = [];
    this.subscribes = [];
    this.subscribeQueryParams = this.route
      .queryParams
      .subscribe(params => {
        if (params.flat) {
          this.flat = params.flat;
          this.onSelectFlat();
        }
      });
  }

  ngOnDestroy(): void {
    this.unSubscribe();
    this.subscribeQueryParams.unsubscribe();
  }

  onSelectFlat(): void {
    this.errorText = '';
    if (this.flat) {
      this.thermostatService.allByFlat(this.flat).subscribe(
        (result: Array<ThermostatReading>) => {
          this.unSubscribe();
          this.thermostats = result;
          result.length ? this.setSubscribes() : this.errorText = 'Flat is not found';
        }
      );
    }
  }

  setSubscribes(): void {
    this.subscribes = [];
    this.thermostats.map<Observable<Reading>>((item: ThermostatReading) => {
      return this.readingService.oneByThermostatIdTimer(item.id);
    }).map((subscribe: Observable<Reading>) => {
      this.subscribes.push(
        subscribe.subscribe(
          (data: Reading) => {
            this.thermostats.map((thermostat: ThermostatReading) => {
              if (thermostat.id === data.thermostat_id) {
                thermostat.reading.temperature = data.temperature;
                thermostat.reading.humidity = data.humidity;
                thermostat.reading.battery_charge = data.battery_charge;
              }
            });
            // Save reading history
            this.readingService.save(data).subscribe(
              (result: boolean) => {
                console.log('Save Reading', result);
              }
            );
          }
        )
      );
    });
  }

  private unSubscribe(): void {
    if (this.subscribes.length) {
      this.subscribes.map((subscribe) => subscribe.unsubscribe());
    }
  }

  public onHistory(id: string): void {
    this.router.navigate(['measurements/' + id]);
  }
}
