import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ThermostatService } from '../../services/thermostat.service';
import { ReadingService } from '../../services/reading.service';
import { AuthService, Roles } from '../../services/auth.service';
import { Thermostat } from '../../interfaces/thermostat';
import { Reading } from '../../interfaces/reading';
import { Pagination } from '../../interfaces/main';

@Component({
  selector: 'app-manager-measurements-page-component',
  templateUrl: './measurements.component.html'
})
export class MeasurementsComponent implements OnInit{

  public title = 'Measurements';
  id: string;
  thermostat: Thermostat;
  reading: Array<Reading>;

  currentPage: number;
  perPage: number;
  total: number;
  totalPages: number;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private thermostatService: ThermostatService,
              private readingService: ReadingService,
              private authService: AuthService) {}

  ngOnInit(): void {
    this.currentPage = 1;
    this.perPage = 10;
    this.total = null;
    this.totalPages = null;
    this.id = this.route.snapshot.paramMap.get('id');
    this.getThermostat();
    this.getReadings(this.currentPage, this.perPage);
  }

  private getThermostat(): void {
    this.thermostatService.one(this.id).subscribe(
      (data: Thermostat) => {
        this.thermostat = data;
      }
    );
  }

  private getReadings(page, perPage): void {
    this.readingService.allByThermostatId(this.id, page, perPage).subscribe(
      (data: Pagination<Reading>) => {
        this.reading = data.data;
        this.currentPage = data.currentPage;
        this.perPage = data.perPage;
        this.total = data.total;
        this.totalPages = data.totalPages;
      }
    );
  }

  public onCancel(): void {
    if (this.authService.getRole() === Roles.User) {
      this.router.navigate(['user'], {queryParams: {flat: this.thermostat.household_token}});
    } else {
      this.router.navigate(['manager']);
    }
  }

  public onPrevPage = () => {
    if (this.currentPage > 1) {
      this.getReadings(this.currentPage - 1, this.perPage);
    }
  }

  public onNextPage = () => {
    if (this.currentPage < this.totalPages) {
      this.getReadings(this.currentPage + 1, this.perPage);
    }
  }

  public onGoToPage = (page: number) => {
    if (this.currentPage !== page) {
      this.getReadings(page, this.perPage);
    }
  }
}
