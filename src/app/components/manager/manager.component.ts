import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Thermostat } from '../../interfaces/thermostat';
import { ThermostatService } from '../../services/thermostat.service';
import { faEye, faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-manager-page-component',
  templateUrl: './manager.component.html'
})
export class ManagerComponent implements OnInit {

  public faEye = faEye;
  public faTrashAlt = faTrashAlt;
  public data: Array<Thermostat>;
  constructor(private router: Router,
              private thermostatService: ThermostatService) {
  }

  ngOnInit(): void {
    this.data = [];
    this.getData();
  }

  public getData(): void {
    this.thermostatService.all().subscribe(
      (result: Array<Thermostat>) => {
        this.data = result;
      }
    );
  }

  public onAddThermostat(): void {
    this.router.navigate(['thermostat/add']);
  }

  onDelete(id: string): void {
    this.thermostatService.delete(id).subscribe(
      (result: boolean) => {
        if (result) {
          this.getData();
        }
      }
    );
  }

  onView(id): void {
    this.router.navigate(['measurements/' + id]);
  }
}
