import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { SelectOption } from '../../../../interfaces/main';
import { ThermostatService, Locations } from '../../../../services/thermostat.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager-add-thermostat-page-component',
  templateUrl: './add-thermostat.component.html'
})
export class AddThermostatComponent implements OnInit{
  public title = 'Create Thermostat';
  public locations: Array<SelectOption>;

  public form: FormGroup = new FormGroup({
    household_token: new FormControl(null, Validators.required),
    location: new FormControl(Locations.LivingRoom, Validators.required),
  });

  constructor(private thermostatService: ThermostatService,
              private router: Router) {}

  ngOnInit(): void {
    this.locations = Object.entries(Locations).map<SelectOption>(e => ({value: e[1], title: e[1]}));
  }

  onSubmit(formGroupDirective: FormGroupDirective): void {
    if (formGroupDirective.form.valid) {
      this.thermostatService.save(formGroupDirective.form.value).subscribe(
        (save) => {
          if (save) {
            this.onCancel();
          }
        });
    }
  }

  onCancel(): void {
    this.router.navigate(['manager']);
  }
}
