import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagerComponent } from './components/manager/manager.component';
import { UserComponent } from './components/user/user.component';
import { AddThermostatComponent } from './components/manager/components/add/add-thermostat.component';
import { ManagerGuardService } from './services/manager-guard.service';
import { MeasurementsComponent } from './components/measurements/measurements.component';

const routes: Routes = [
  {
    path: 'manager',
    canActivate: [ManagerGuardService],
    component: ManagerComponent,
  }, {
    path: 'thermostat/add',
    canActivate: [ManagerGuardService],
    component: AddThermostatComponent
  }, {
    path: 'measurements/:id',
    component: MeasurementsComponent
  }, {
    path: 'user',
    component: UserComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
