import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{

  title = 'Central Heating Control';
  links = [
    { title: 'Manager', url: 'manager' },
    { title: 'User', url: 'user' }
  ];

  constructor(public route: ActivatedRoute,
              private router: Router,
              private auth: AuthService) {}

  ngOnInit(): void {
    this.router.navigate(['']);
  }

  public onSelectRole(urlRole: string): void {
    this.auth.setRole(urlRole);
    this.router.navigate([urlRole]);
  }
}
