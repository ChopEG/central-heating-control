export interface ThermostatForm {
  household_token: number; // float
  location: string;
}

export interface Thermostat extends ThermostatForm {
  id: string;
}

export interface ThermostatReading extends Thermostat {
  reading: {
    temperature: number;
    humidity: number;
    battery_charge: number;
  };
}
