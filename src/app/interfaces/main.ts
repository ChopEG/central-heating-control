export interface SelectOption {
  value: string;
  title: string;
}

export interface Pagination<T> {
  currentPage: number;
  perPage: number;
  total: number;
  totalPages: number;
  data: Array<T>;
}
