import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';

import { AuthService } from './services/auth.service';
import { ManagerGuardService } from './services/manager-guard.service';
import { ThermostatService } from './services/thermostat.service';
import { ReadingService } from './services/reading.service';

import { UserComponent } from './components/user/user.component';
import { ManagerComponent } from './components/manager/manager.component';
import { AddThermostatComponent } from './components/manager/components/add/add-thermostat.component';
import { MeasurementsComponent } from './components/measurements/measurements.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    ManagerComponent,
    AddThermostatComponent,
    MeasurementsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
  ],
  providers: [
    AuthService,
    ManagerGuardService,
    ThermostatService,
    ReadingService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
